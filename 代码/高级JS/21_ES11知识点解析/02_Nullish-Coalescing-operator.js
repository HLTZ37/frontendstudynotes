// ES11: 空值合并运算 ??

const foo = undefined
// const bar = foo || "default value"   // ‘||’运算符无法判断 在foo 取值 为0/"" 的情况下依旧使用 0/""，但是?? 运算符可以
const bar = foo ?? "defualt value"

console.log(bar)

// ts 是 js 的超级