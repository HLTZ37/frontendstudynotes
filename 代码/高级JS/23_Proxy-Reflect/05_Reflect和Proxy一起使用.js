const obj = {
  name: "why",
  age: 18
}

const objProxy = new Proxy(obj, {
  get: function (target, key, receiver) {
    console.log("get---------")
    return Reflect.get(target, key)
  },
  set: function (target, key, newValue, receiver) {
    console.log("set---------")
    // target[key] = newValue  // 原来的这种方式无法得知的是否设置成功

    const result = Reflect.set(target, key, newValue) // Reflect可以返回一个Boolean值，设置成功就返回true,否则返回false,功能丰富了一些
    // if (result) {} else {}    // 然后就可以根据是否设置成功进行一些其他的操作
  }
})

objProxy.name = "kobe"
console.log(objProxy.name)