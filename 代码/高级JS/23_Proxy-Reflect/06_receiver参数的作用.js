const obj = {
  _name: "why",
  get name() {
    return this._name
  },
  set name(newValue) {
    this._name = newValue
  }
}
/**
 * 自己的理解：
 * objProxy可以理解为obj的备份（ 代理）， 所以使用objProxy.name访问属性name的时候，应该访问的是objProxy的name，而不是原对象obj 的name
 * 当没有参数receiver的情况下， obj中的this指的是obj, 在有receiver的情况下， Reflect会改变this的指向为receiver,receiver指的是代理objProxy
 * receiver就用用来改变this的
 * 
 * 疑问1： 用receiver来改变this指向，是在Proxy中实现的，还是在Reflect中实现的？
 */

const objProxy = new Proxy(obj, {
  get: function (target, key, receiver) {
    // receiver是创建出来的代理对象
    console.log("get方法被访问--------", key, receiver)
    console.log(receiver === objProxy)
    return Reflect.get(target, key, receiver)
  },
  set: function (target, key, newValue, receiver) {
    console.log("set方法被访问--------", key)
    Reflect.set(target, key, newValue, receiver)
  }
})

// console.log(objProxy.name)
objProxy.name = "kobe"