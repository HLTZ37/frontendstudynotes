function Student(name, age) {
  this.name = name
  this.age = age
}

function Teacher() {

}

// const stu = new Student("why", 18)
// console.log(stu)
// console.log(stu.__proto__ === Student.prototype)

// 执行Student函数中的内容, 但是创建出来对象是Teacher对象
const teacher = Reflect.construct(Student, ["why", 18], Teacher)
console.log(teacher)
console.log(teacher.__proto__ === Teacher.prototype)


// function inheritPrototype(SubType, SuperType) {
//   SubType.prototype = Objec.create(SuperType.prototype) // (2)
//   Object.defineProperty(SubType.prototype, "constructor", { // 将创建出来的子类的类型指定为子类，不指定的话打印输出的是父类
//     enumerable: false,
//     configurable: true,
//     writable: true,
//     value: SubType // （*1*）
//   })
// }