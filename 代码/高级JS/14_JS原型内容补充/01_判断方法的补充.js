// 原型
var obj = {
  name: "why",
  age: 18
}
// Object.create(obj)  该语句会创建一个对象，将此对象的原型指向obj，并返回该对象。所以这里info的原型指向obj,但是obj 的原型应该是Object对象
var info = Object.create(obj, {
  address: {
    value: "北京市",
    enumerable: true
  } // 这个属性会被添加到当前对象中，而不是原型上
})

// hasOwnProperty方法判断 判断该属性是当前对象的还是原型的
// console.log(info.hasOwnProperty("address"))
// console.log(info.hasOwnProperty("name"))  
console.log(info)

// in 操作符: 不管在当前对象还是原型中返回的都是true
// console.log("address" in info)
// console.log("name" in info)

// // for in
// for (var key in info) {
//   console.log(key)
// }

// console.log(info.__proto__ == obj); // true
// console.log(info.__proto__);
// console.log(obj.__proto__);