//  instanceof  用于检测构造函数的pototype， 是否出现在某个实例对象的原型链上
// isPrototypeOf 用于检测某个对象， 是否出现在某个实例对象的原型链上

function Person() {

}

var p = new Person()

// instanceof 的右边是构造函数 ，而isPrototypeOf是
console.log(p instanceof Person)
console.log(Person.prototype.isPrototypeOf(p))

// 
var obj = {
  name: "why",
  age: 18
}

var info = Object.create(obj)

// console.log(info instanceof obj)
console.log(obj.isPrototypeOf(info))