var obj = {
  name: "why",
  age: 18
}

var info = Object.create(obj) // ECMA的实现方式，// Object.create(obj)  该语句会创建一个新对象，将此对象的原型指向obj，并返回该对象

// 原型式继承函数，也就是Object.create()内部的实现的，可以看出，返回这个对象后，这个创建的对象会被销毁

// 原型式继承方式一
function createObject1(o) {
  var newObj = {}
  Object.setPrototypeOf(newObj, o); // 将o作为newObj的原型 setPrototypeOf()函数的作用
  // newObj.__proto__ = o;  一般不这么做
  return newObj
}

// 原型式继承 方式二 
function createObject2(o) {
  function Fn() {}
  Fn.prototype = o
  var newObj = new Fn()
  return newObj
}

// ECMA 已实现的方式，内部本质做的是和上面的方式一和方式二一致。
// var info = createObject2(obj)
var info = Object.create(obj)
console.log(info)
console.log(info.__proto__)