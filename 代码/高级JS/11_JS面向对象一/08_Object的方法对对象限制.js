var obj = {
  name: 'why',
  age: 18
}
// 0.获取对象的属性描述符
// 获取某一个特性属性的属性描述符
console.log(Object.getOwnPropertyDescriptor(obj, "name"))
console.log(Object.getOwnPropertyDescriptor(obj, "age"))
// 获取对象的所有属性描述符
console.log(Object.getOwnPropertyDescriptors(obj))

// 1.禁止对象继续添加新的属性
Object.preventExtensions(obj)

obj.height = 1.88
obj.address = "广州市"

console.log(obj)

// 2.禁止对象配置/删除里面的属性

// 常规实现方式
// for (var key in obj) {
//   Object.defineProperty(obj, key, {
//     configurable: false,
//     enumerable: true,
//     writable: true,
//     value: obj[key]
//   })
// }

// 对象的实现方式
Object.seal(obj) // 使得对象中的所有属性不可配置

delete obj.name // 操作无效，在严格模式下会报错
console.log(obj.name) // undefined

// 3.让属性不可以修改(writable: false)
Object.freeze(obj)

obj.name = "kobe"
console.log(obj.name)