/**
 * js模拟apply的实现
 * apply和call 的唯一区别是，剩余参数一数组的形式传递，也就是多余的都会包含的在一个数组中传递
 * apply整体只有两个参数，一个是需要绑定的this,一个是原函数需要的参数
 */

function foo(num1, num2) {
  console.log(this);
  // console.log(num1 + num2);
  console.log(num1 + num2);

}
// 一、系统的实现方式
// foo.apply(0, [1, 2]);
// foo.apply(0, [1, 2], 12, 15); // 多余的参数无效


// 二、自己实现apply的方法  selfApply()
/**
 * 复现apply思路：
 * 需要实现的目标：
 * 1.改变调用函数的this指向
 * 2.第一个参数表示需要绑定的this的指向，第二个参数是个数组，apply将需要传递原函数的所有参数放在数组中，除此之外的多余参数无效
 * 3.将第二个参数（数组）中的元素传递到原函数中执行
 * 4.边界情况
 *  4.1 当传递的第一个参数是： undefined/null 时，this指向全局
 * 5. 其他注意
 *  要对传入的this参数转换成对象的形式，因为原始数据类型没有对象的引用方式，会报错， 所以需要转换成对象的方式
 */
/**
 * 官方 参数 注释
 * @param thisArg The object to be used as the current object.
 * @param argArray A list of arguments to be passed to the method.
 */
function selfApply(thisArg, argsArray) {
  // 1.获取调用本函数的函数，也就是发起者
  var applyFun = this;

  // 2. 边界判断,对象转换参数数组的判断
  thisArg = (thisArg != undefined && thisArg != null) ? Object(thisArg) : window; //(*重点*)，要抓换成对象的形式，因为原始数据类型没有对象的引用方式，会报错，所以需要转换成对象的方式
  argsArray = argsArray ? argsArray : []

  // 3. 改变该函数的this指向
  thisArg.applyFun = applyFun; // (*重点*) 采用隐式绑定的方法，改变this的指向
  thisArg.applyFun(...argsArray); // (*重点*) 调用执行改变this指向后的原函数


  //4.如果需要在需要返回指的情况下可以返回
  // var result = thisArg.applyFun(...argsArray); // (*重点*) 调用执行改变this指向后的原函数
  // return result

  // 5.
  // delete thisArg.applyFun  // 删除

}
Function.prototype.selfApply = selfApply; // 需要将自定义的call函数添加到函数原型上，否则其他所有函数无法使用自定义的call
foo.selfApply("klkl", [1, 2]); // 如果原函数值接受两个参数，多余的参数的就不会生效

// //  有返回值的调用
// var res = foo.selfApply("sss", [1, 2]);
// console.log(res);

/**
 * 知识点补充：
 * 1. 展开运算符号：...argsArray
 * 2. argArray = argArray || [] ， || 运算符的使用
 */