/**
 * js模拟call的实现
 */

function foo(num1, num2) {
  console.log(this);
  // console.log(num1 + num2);
  return num1 + num2

}
// 一、系统的实现方式
// foo.call(0, 1, 2);


// 二、自己实现call的方法  selfCall()
/**
 * 复现call思路：
 * 需要实现的目标：
 * 1.改变调用函数的this指向
 * 2.可以接受多个参数。即可以接收除了第一个this绑定的参数外，还以接收其他参数。
 *   传多余的参数的是因为在原函数本来需要传参的情况下，当调用call时，会将需要传递的参数，添加在第一参数后面，以逗号隔开
 * 3.将剩余的参数（除了第一个参数外的）传递到原函数中执行
 * 4.边界情况
 *  4.1 当传递的第一个参数是： undefined/null 时，this指向全局
 * 5. 其他注意
 *  要对传入的this参数转换成对象的形式，因为原始数据类型没有对象的引用方式，会报错， 所以需要转换成对象的方式
 */
/**
 * 这是官方注释
 * @param thisArg The object to be used as the current object.
 * @param argArray A list of arguments to be passed to the method.
 */
function selfCall(thisArg, ...argsArray) {
  // 1.获取调用本函数的函数，也就是发起者
  var callFun = this;

  // 2. 边界判断和对象转换
  thisArg = (thisArg != undefined && thisArg != null) ? Object(thisArg) : window; //(*重点*)，要抓换成对象的形式，因为原始数据类型没有对象的引用方式，会报错，所以需要转换成对象的方式

  // 3. 改变该函数的this指向
  thisArg.callFun = callFun; // (*重点*) 采用隐式绑定的方法，改变this的指向
  thisArg.callFun(...argsArray); // (*重点*) 调用执行改变this指向后的原函数


  //4.如果需要在需要返回指的情况下可以返回
  // var result = thisArg.callFun(...argsArray); // (*重点*) 调用执行改变this指向后的原函数
  // delete thisArg.callFun  // 又返回指的情况下，可以删除
  // return result


}
Function.prototype.selfCall = selfCall; // 需要将自定义的call函数添加到函数原型上，否则其他所有函数无法使用自定义的call

foo.selfCall("klkl", 2, 3, 4); // 如果原函数值接受两个参数，多余的参数的就不会生效

// var res = foo.selfCall("aaa", 1, 2, 3); // 
// console.log(res);


/**
 * 知识点补充：
 * 1. 展开运算符号：...argsArray
 *
 */