/**
 * js模拟bind的实现
 * bind不同于apply和call 是，bind会返回的一个函数，参数的形式同call,第一个参数是需要绑定的this,剩余的参数则是需要想原函数传递的参数
 */

function foo(num1, num2) {
  console.log(this);
  // console.log(num1 + num2);
  console.log(num1 + num2);

}

function bar(num1, num2, num3, num4) {
  console.log(this);
  console.log(num1, num2, num3, num4);

}
// 一、系统的实现方式
// var retFn = foo.bind("sds", 1, 2);
// retFn();

// foo.apply(0, [1, 2], 12, 15); // 多余的参数无效


// 二、自己实现apply的方法  selfApply()
/**
 * 复现apply思路：
 * 需要实现的目标：
 * 1.改变调用函数的this指向
 * 2.可以接受多个参数。
 *   即可以接收除了第一个this绑定的参数外，还以接收其他参数。
 *   传多余的参数的是因为在原函数本来需要传参的情况下，当调用call时，会将需要传递的参数，添加在第一参数后面，以逗号隔开
 * 3.返回值是一个函数,返回的函数任然可以传递参数，也就是最后的原函数执行的参数来自两部分，一部分是bind中传递的，一部分是返回的函数触传递的
 * 4.边界情况
 *  4.1 当传递的第一个参数是： undefined/null 时，this指向全局
 * 5. 其他注意
 *  要对传入的this参数转换成对象的形式，因为原始数据类型没有对象的引用方式，会报错， 所以需要转换成对象的方式
 */
/**
 * 官方 参数 注释
 * @param thisArg The object to be used as the current object.
 * @param argArray A list of arguments to be passed to the function.
 */
function selfBind(thisArg, ...argsArray1) {

  // 1.获取调用本函数的函数，也就是发起者
  var bindFun = this;
  return function (...argsArray2) {
    // 2. 边界判断,对象转换参数数组的判断
    thisArg = (thisArg != undefined && thisArg != null) ? Object(thisArg) : window; //(*重点*)，要抓换成对象的形式，因为原始数据类型没有对象的引用方式，会报错，所以需要转换成对象的方式
    // 3. 改变该函数的this指向
    thisArg.bindFun = bindFun; // (*重点*) 采用隐式绑定的方法，改变this的指向
    thisArg.bindFun(...argsArray1, ...argsArray2); // (*重点*) 调用执行改变this指向后的原函数

    // 4.删除
    delete thisArg.bindFun
  }

}
Function.prototype.selfBind = selfBind; // 需要将自定义的call函数添加到函数原型上，否则其他所有函数无法使用自定义的call
var retBin = foo.selfBind("bind", 1, 2); // 如果原函数值接受两个参数，多余的参数的就不会生效
retBin();

var barBin = bar.selfBind("bindBar1", 1, 2, 3, 4);
barBin()
var barBin2 = bar.selfBind("bindBar2", 1, 2);
barBin(3, 4)