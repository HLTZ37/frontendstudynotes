const obj = {
  name: "why",
  age: 18,
  friends: {
    name: "kobe"
  },
  hobbies: ["篮球", "足球"],
  foo: function () {
    console.log("foo function")
  }
}

// 将obj对象的内容放到info变量中
// 1.引用赋值
const info = obj
obj.age = 100
console.log(info.age)

// 2.浅拷贝
const info2 = {
  ...obj
} // 这种方式只保证了info2 和obj 及其内部第一层原型数据类型 被开拷贝（拥有不同的地址）
obj.age = 1000
console.log(info2.age)

obj.friends.name = "james"
// 浅拷贝只能实现第一层的基础类数据类型的拷贝，如果第一层数据数有数组函数等引用型数据类型，则不发实现拷贝
console.log(info2.friends.name) // info2 和obj 有相同的值，都会被改变

// 3.stringify和parse来实现 很深拷贝。但是注意对函数无效
const jsonString = JSON.stringify(obj)
console.log(jsonString)
const info3 = JSON.parse(jsonString)
obj.friends.name = "curry"
console.log(info3.friends.name)
console.log(info3)