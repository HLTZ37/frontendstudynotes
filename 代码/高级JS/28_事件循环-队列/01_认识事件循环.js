console.log("script start")

// 业务代码  setTimeout函数本省不是一部函数，而是同步函数，其里面的回调函数才是异步函数，也就是在n秒之后被执行
setTimeout(function () {
  //  计时操作由浏览器中的其他线程完成
  // 当计时结束后会将这个任务放在一个由浏览器维护的任务队列中，然后JS按照队列的性质一次执行里面的任务
}, 1000)

console.log("后续代码~")


console.log("script end")
//  事件队列是将计时到时的任务放在一个队列中让JS线程执行，这样的任务被叫做微任务