console.log(window.location)

// 当前的完整的url地址
console.log(location.href)

// 协议protocol
console.log(location.protocol)

// 几个方法

//  打开新的链接
// location.assign("http://www.baidu.com")  // 可以回退
// location.href = "http://www.baidu.com"   

// location.replace("http://www.baidu.com")   // 不可以回退
// location.reload(false)

// Vnode是用JS模拟DOM