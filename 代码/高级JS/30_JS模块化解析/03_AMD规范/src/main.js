// 要预定配置好映射
require.config({
  baseUrl: '',
  // baseUrl的默认值是当前文件所在的路径
  paths: {
    foo: "./src/foo",
    bar: "./src/bar"
  }
})


require(["foo", "bar"], function (foo) { // 加载模块["foo"和 "bar"],
  console.log("main:", foo)
})