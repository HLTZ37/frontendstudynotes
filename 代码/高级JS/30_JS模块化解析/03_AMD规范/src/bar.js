// 在定义bar模块时就声明已经依赖foo模块
define(["foo"], function (foo) {
  console.log("--------")
  // require(["foo"], function(foo) {
  //   console.log("bar:", foo)
  // })

  console.log("bar:", foo)
  // 不同过上面的方式也可以直接使用
})