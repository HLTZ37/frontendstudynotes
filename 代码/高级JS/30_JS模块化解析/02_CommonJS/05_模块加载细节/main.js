console.log("main.js代码开始运行")

require("./foo") // 只会被加载一次（requires执行时会运行一次该模块的代码），然后缓存
require("./foo")
require("./foo")

console.log("main.js代码后续运行")