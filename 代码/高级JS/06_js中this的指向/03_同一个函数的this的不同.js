// this指向什么, 跟函数所处的位置是没有关系的
// 跟函数被调用的方式是有关系
/**
 * 一、
 * 1.函数在调用的手，javascript会默认给this一个绑定的值
 * 2. this的绑定和定义的位置（编写的位置）没有关系
 * 3. this 的绑定和调用的方式和调用的位置有关系
 * 4. this 是在运行是被绑定的
 * 二、this的绑定规则：
 * 1.默认绑定
 * 2.隐式绑定
 * 3.显示绑定
 * 4.new绑定
 * 
 */

function foo() {
  console.log(this)
}

// 以下是三种不同的函数调用方式
// 1.直接调用这个函数
foo()

// 2.创建一个对象, 对象中的函数指向foo
var obj = {
  name: 'why',
  foo: foo
}

obj.foo()

// 3.apply调用
foo.apply("abc")