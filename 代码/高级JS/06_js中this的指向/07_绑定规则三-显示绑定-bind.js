/**
 * 总结：
 * bind也是显示绑定，
 * 用处在于，如果想一个函数的this一直绑定某一个对象,就需要在每次调用时都需要使用call或apply绑定，这种方式比较麻烦
 * 所以， 使用使用bind重新赋值的方式， 如：var newFoo = foo.bind("aaa")，再使用独立绑定的方式调用，
 * 这样就一直绑定了前面需要的绑定的对象，而不需要多次绑定，演示如下
 * 
 */
function foo() {
  console.log(this)
}

// foo.call("aaa")
// foo.call("aaa")
// foo.call("aaa")
// foo.call("aaa")

// 默认绑定和显示绑定bind冲突: 优先级(显示绑定)
var newFoo = foo.bind("aaa")

newFoo()
newFoo()
newFoo()
newFoo()
newFoo()
newFoo()

var bar = foo // 对象的引用赋值
console.log(bar === foo) // true
console.log(newFoo === foo) // false