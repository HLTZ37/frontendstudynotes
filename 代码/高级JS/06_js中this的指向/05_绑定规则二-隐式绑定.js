// 隐式绑定: object.fn()
// object对象会被js引擎绑定到fn函数的中this里面

function foo() {
  console.log(this)
}

// foo() //  这是独立函数调用

// 1.案例一:
// var obj = {
//   name: "why",
//   foo: foo
// }

// obj.foo() // obj对象

// 2.案例二:
// var obj = {
//   name: "why",
//   eating: function() {
//     console.log(this.name + "在吃东西")
//   },
//   running: function() {
//     console.log(obj.name + "在跑步")
//   }
// }

// // obj.eating()  // 这种情况下，this 指向的是obj，name会被正常打印 >> why 在吃东西；这里的这种方式隐式绑定
// // obj.running()

// var fn = obj.eating
// fn()  // 这种情况下，this指向的是window,但是 window没有name这个属性，但是仍然会正常执行，这里的这种方式默认绑定，


// 3.案例三:
var obj1 = {
  name: "obj1",
  foo: function () {
    console.log(this)
    console.log(this.name);
  }
}

var obj2 = {
  name: "obj2",
  bar: obj1.foo
}

obj2.bar() // 隐式绑定，调用由obj2发起，所以this指向ibj2，同样访问的namse是obj2中的name