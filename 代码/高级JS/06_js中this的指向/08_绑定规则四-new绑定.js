/**
 * new的绑定方式总结：
 *  我们通过一个new关键字调用一个函数时(构造器), 这个时候this是在调用这个构造器时创建出来的对象
 *  this = 创建出来的对象
 *  这个绑定过程就是new 绑定
 * 
 */

function Person(name, age) {
  this.name = name;
  this.age = age;
  this.foo = function () {
    console.log(this);
  }
  console.log("最外层", this);
}
// new的时候会执行Person里面的代码，比如有log输出的时候，会执行该输出

var p1 = new Person("why", 18) //  >> 最外层 Person { name: 'kobe', age: 30, foo: [Function (anonymous)] }
console.log(p1.name, p1.age, p1.foo) // why 18 [Function (anonymous)]     

console.log("----------------------------------");

var p2 = new Person("kobe", 30) // >>  最外层 Person { name: 'kobe', age: 30, foo: [Function (anonymous)] }


var obj = {
  foo: function () {
    console.log(this)
  }
}
obj.foo() // 隐式绑定，this 指的是obj