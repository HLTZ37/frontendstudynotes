// 在大多数情况下, this都是出现在函数中
// 在全局作用域下
// 在浏览器中: this 指的是：window(globalObject) // 在浏览器中执行
// 在Node环境:this 指向的是： {}  // 在node中执行，node 中没有window,只有在浏览器中才有window对象
console.log(this)
//  在浏览器中运行
// console.log(window)
console.log(this == window) // >>  true