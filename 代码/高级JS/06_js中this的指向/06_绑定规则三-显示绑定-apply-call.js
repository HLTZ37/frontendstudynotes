/**
 * 总结：
 * 一、apply和call 的区别主要在传递参数的不一样，
 *  1.call 是以剩余参数的形式传递参数，也就是参数之间用逗号隔开
 *  2.apply 是以数组的形式传递参数，将参数都同意写在数组中，
 * 二、相同之处
 *  两者都可以明确的指定this 的指向，也就是显示绑定this。this甚至还是指向某一个字符串，如下。
 * 此外， apply和call的第一个参数指的是this要显示绑定的对象
 */

function foo() {
  console.log("函数被调用了", this)
}

// 1.foo直接调用和call/apply调用的不同在于this绑定的不同
// foo直接调用指向的是全局对象(window)
// foo()

var obj = {
  name: "obj"
}

// call/apply是可以指定this的绑定对象 
foo.call(obj)
foo.apply(obj)
foo.apply("aaaa")


// 2.call和apply有什么区别?
function sum(num1, num2, num3) {
  console.log(num1 + num2 + num3, this)
}

sum.call("call", 20, 30, 40)
sum.apply("apply", [20, 30, 40])

// 3.call和apply在执行函数时,是可以明确的绑定this, 这个绑定规则称之为显示绑定