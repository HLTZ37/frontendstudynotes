var name = "why"
var age = 18

var obj = {
  // 1.property shorthand(属性的简写)
  name,
  age,

  // 2.method shorthand(方法的简写)
  foo: function () { // (1)
    console.log(this)
  },
  bar() { // (2)  注意：（2）是（1）的字面量增强写法，但是（2）不是（3）的增强写法，（3）是箭头函数，this的绑定规则不一样
    console.log(this)
  },
  baz: () => { // (3)
    console.log(this)
  },

  // 3.computed property name(计算属性名)
  [name + 123]: 'hehehehe'
}

obj.baz()
obj.bar()
obj.foo()

// obj[name + 123] = "hahaha"
console.log(obj)