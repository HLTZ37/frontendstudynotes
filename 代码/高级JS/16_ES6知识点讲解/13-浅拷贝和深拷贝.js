// Object.assign()实现浅拷贝
let obj1 = {
  person: {
    name: "kobe",
    age: 41
  },
  sports: 'basketball'
};
let obj2 = Object.assign({}, obj1);
let obj3 = obj1;
obj2.person.name = "wade";
obj2.sports = 'football'

console.log(obj1 == obj2); // false
console.log(obj1 == obj3); // true
console.log(obj1.person == obj2.person) // true
console.log(obj1); // { person: { name: 'wade', age: 41 }, sports: 'basketball' }