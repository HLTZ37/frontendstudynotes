// async -await 是将异步代码写成同步的形式
// 1.await跟上表达式
function requestData() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // resolve(222)
      reject(1111) // 
    }, 2000);
  })
}

// async function foo() {
//   const res1 = await requestData()   // 只有await 后面的表达值拿到结果后才会执行下面的代码，这种效果就相当返回解雇回调then 继续执行后面的代码
//   console.log("后面的代码1", res1)  //  后面的的这三句相当于是在 await requestData()之后调用then之后的执行，因为只有在await 后的表达式执行拿到结果之后才会执行后面的
//   console.log("后面的代码2")
//   console.log("后面的代码3")

//   const res2 = await requestData()
//   console.log("res2后面的代码", res2)
// }

// 2.跟上其他的值
// async function foo() {
//   // const res1 = await 123 ; // 普通值会直接返回。res1这里拿到直接是值，（好像不是promise）
//   // const res1 = await {   // 如果await 后面的promise或者实现thenablede 对象内部执行了reject，那么会将这个异常作为整个异步函数的异常返回，并结束
//   //   then: function(resolve, reject) {
//   //     resolve("abc")
//   //   }
//   // }
//   const res1 = await new Promise((resolve) => {
//     resolve("why")
//   })
//   console.log("res1:", res1)
// }

// 3.reject值
async function foo() {
  const res1 = await requestData()
  console.log("res1:", res1)
}

foo().catch(err => {
  console.log("err:", err)
})