/**
 * 总结：
 * 1.异步函数的返回值一定是一个promise
 * 
 */
async function foo() {

  console.log("foo function start~")

  console.log("中间代码~")

  console.log("foo function end~")

  // 1.返回一个值

  // 2.返回thenable
  // return {
  //   then: function(resolve, reject) {
  //     resolve("hahahah")  // 当return的返回值是一个实现thenable的对象时，对res将回执 resolve或者catch 中的参数
  //   }
  // }

  // 3.返回Promise
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("hehehehe")
    }, 2000)
  })
}

// 异步函数的返回值一定是一个Promise
const promise = foo()
promise.then(res => { // then是在foo中 执行return 后被执行，then za在执行时是被执行时，是被加在微任务队列中的
  console.log("promise then function exec:", res) // 这里的res指的是在异步函数内部执行return的时的返回值，
  // 当异步函数中没有return 语句时，实际默认执行了 return undefined
})