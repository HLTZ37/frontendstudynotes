async function foo() {
  console.log("foo function start~")

  console.log("中间代码~")

  // 异步函数中的异常, 会被作为异步函数返回的Promise的reject值的
  throw new Error("error message") // 异步函数foo内部以及外部的大司马，在没有捕获处理异常时 在此之后的代码后都不会被执行，就终止了
  // 异步函数会将这种个异常传递给foo函数的catch 方法， 因为async 声明的函数的返回值是一个promise，所以这个函数有then 和 catch 方法

  console.log("foo function end~")
}

// 异步函数的返回值一定是一个Promise
foo().catch(err => { // 在这里捕获处理异常，因为async 声明的函数的返回值是一个promise，所以这个函数有then 和 catch 方法
  console.log("coderwhy err:", err)
})

console.log("后续还有代码~~~~~")