var singleNumber = function (nums) {
  let res = 0;
  nums.forEach(x => {
    console.log("前：", res, '=>', x)
    res ^= x;
    console.log("后：", res, '=>', x)
    console.log("------------------")
  })
  return res;
};

let arr = [4, 1, 2, 1, 2];
singleNumber(arr)