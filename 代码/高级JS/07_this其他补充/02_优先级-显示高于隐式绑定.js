// var obj = {
//   name: "obj",
//   foo: function() {
//     console.log(this)
//   }
// }

// obj.foo()

// 1.call/apply的显示绑定高于隐式绑定
// obj.foo.apply('abc')  // 这里obj.foo是隐式绑定，但是后面又使用了显示绑定call()，这是隐式绑定和显示绑定共存，这种情况下，显示绑定优先级高于隐式绑定
// obj.foo.call('abc')  // 同上

// 2.bind的优先级高于隐式绑定
// var bar = obj.foo.bind("cba")
// bar()


// 3.更明显的比较  
function foo() {
  console.log(this)
}

var obj = {
  name: "obj",
  foo: foo.bind("aaa")
}

obj.foo()