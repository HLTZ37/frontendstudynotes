var name = 'window'

function Person(name) {
  this.name = name
  this.foo1 = function () {
      console.log(this.name)
    },
    this.foo2 = () => console.log(this.name),
    this.foo3 = function () {
      return function () {
        console.log(this.name)
      }
    },
    this.foo4 = function () {
      return () => {
        console.log(this.name)
      }
    }
}

var person1 = new Person('person1')
var person2 = new Person('person2')

person1.foo1() // >> person1 隐式绑定
person1.foo1.call(person2) // >> person2  显式绑定

person1.foo2() // >> person1  箭头函数没有绑定的this，需要从上层作用域中寻找，其上层作用是函数Person, 绑定的this 是persoon1
person1.foo2.call(person2) // >> person1 箭头函数使用bind/call/apply绑定this无效，其this仍然需要从上层作用域中寻找。其上层作用域仍然是函数Person,其绑定的this指向peron1

person1.foo3()() //　>> window 独立函数执行
person1.foo3.call(person2)() // >> window 独立函数执行。person1.foo3.call(person2)只是为foo3显式绑定了this为person2,但是并没有给返回的函数绑定任何this，返回的函数的this跟其调用的方式的和调用的位置有关系
person1.foo3().call(person2) //  >> person2  显式绑定。因为person1.foo3()为其返回函数执行了call(person2),这是显式绑定

person1.foo4()() // >> person1  返回的箭头函数不绑定this,会去其上层作用域中寻找this,其上层作用域是foo4,使用person1.foo4()执行后，foo4的this绑定person1，这是隐式绑定
person1.foo4.call(person2)() // >> person2  返回的箭头函数不绑定this,会去其上层作用域中寻找this,其上层作用域是foo4,使用person1.foo4.call(person2)执行后，foo4的this绑定person2，这是显式绑定
person1.foo4().call(person2) // >> perosn1,返回的是箭头函数，箭头函数使用bind/call/apply绑定this无效，会去其上层作用域中寻找this,其上层作用域是foo4,使用person1.foo4()执行后，foo4的this绑定person1，这是隐式绑定




// person1.foo1() // person1
// person1.foo1.call(person2) // person2(显示高于隐式绑定)

// person1.foo2() // person1 (上层作用域中的this是person1)
// person1.foo2.call(person2) // person1 (上层作用域中的this是person1)

// person1.foo3()() // window(独立函数调用)
// person1.foo3.call(person2)() // window
// person1.foo3().call(person2) // person2

// person1.foo4()() // person1
// person1.foo4.call(person2)() // person2
// person1.foo4().call(person2) // person1


var obj = {
  name: "obj",
  foo: function () {

  }
}