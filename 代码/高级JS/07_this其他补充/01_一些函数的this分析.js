/**
 * 总结：
 * 1.默认情况下，setTimeout(function(){},time) 内部调用回调函数的方式是独立调用，这就是对这个回调函数的this绑定使用的是默认绑定，this会指向window
 * 2.在高阶函数内部调用的回调函数的方式也是独立直接调用
 * 3. 回调函数本身提供可以改变this 指向的参数: thisArg,如下所示，会将这的参数作为this的绑定。
 *  (method) Array < string > .forEach(callbackfn: (value: string, index: number, array: string[]) => void, thisArg ? : any): void
 * 
 */
// 1.setTimeout
// function hySetTimeout(fn, duration) {
//   fn.call("abc")
// }

// hySetTimeout(function() {
//   console.log(this) // window
// }, 3000)

// setTimeout(function() {
//   console.log(this) // window
// }, 2000)

// // 2.监听点击
// const boxDiv = document.querySelector('.box')
// boxDiv.onclick = function() {
//   console.log(this)
// }

// boxDiv.addEventListener('click', function() {
//   console.log(this)
// })
// boxDiv.addEventListener('click', function() {
//   console.log(this)
// })
// boxDiv.addEventListener('click', function() {
//   console.log(this)
// })

// 3.数组.forEach/map/filter/find
var names = ["abc", "cba", "nba"]
names.forEach(function (item) {
  console.log(item, this)
})
names.map(function (item) {
  console.log(item, this)
}, "cba")