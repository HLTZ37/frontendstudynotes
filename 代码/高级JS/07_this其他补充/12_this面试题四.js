var name = 'window'

function Person(name) {
  this.name = name
  this.obj = {
    name: 'obj',
    foo1: function () {
      return function () {
        console.log(this.name)
      }
    },
    foo2: function () {
      return () => {
        console.log(this.name)
      }
    }
  }
}

var person1 = new Person('person1') // 返回的对象的this 会指向person1
var person2 = new Person('person2') // 返回的对象的this 会指向person2

person1.obj.foo1()() // >>  window ;函数的独立执行，因为person1.obj.foo1()执行后会返回一个匿名函数，之后直接执行了匿名函数
person1.obj.foo1.call(person2)() // >> window; 函数的独立执行，因为person1.obj.foo1.call(person2)执行后,只是为person1.obj中的foo1的this绑定了指向，为person2，并且会返回一个匿名函数，并没有对返回的函数的this指向做任何的操作。之后直接执行了匿名函数
person1.obj.foo1().call(person2) // >> person2; 显式绑定； person1.obj.foo1()返回的函数，为其绑定了person2

person1.obj.foo2()() // obj;  person1.obj.foo2()执行结束后返回箭头函数，箭头函数没有绑定的this，需要从上层作用域中寻找，其上层作用域是foo2，需要注意的是foo2()是由person1中obj发起调用的。所以foo2()中的this绑定属于隐式绑定，指向obj对象                                                  
person1.obj.foo2.call(person2)() // 错误：window 函数的独立执行，person1.obj.foo2.call(person2)执行结束后，只是为personn1中的obj中的foo2显式绑定了this,指向person2,和返回的箭头函数无关 ，而是直接在全局作用域中执行了该返回的箭头函数，所以数据函数的独立执行，即默认绑定                                                                                     
// 上面的正确结果: person2 函数的独立执行， person1.obj.foo2.call(person2) 执行结束后， 只是为personn1中的obj中的foo2显式绑定了this, 指向person2, 和返回的箭头函数无关， 返回的箭头函数需要从上层作用域中寻找， 其上层作用域是foo2， foo2显式绑定了this, 指向person2
person1.obj.foo2().call(person2) // obj; 为person1.obj.foo2()返回的箭头函数执行call绑定this无效，所以需要从上层作用域中寻找，其上层作用域是foo2，foo2的执行是由person1中的obj发起调用的，所以foo2()中this指向obj



















// person1.obj.foo1()() // window
// person1.obj.foo1.call(person2)() // window
// person1.obj.foo1().call(person2) // person2

// person1.obj.foo2()() // obj
// person1.obj.foo2.call(person2)() // person2
// person1.obj.foo2().call(person2) // obj


// 

// 上层作用域的理解
// var obj = {
//   name: "obj",
//   foo: function() {
//     // 上层作用域是全局
//   }
// }

// function Student() {
//   this.foo = function() {

//   }
// }