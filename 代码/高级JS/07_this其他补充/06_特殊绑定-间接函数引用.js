// 争论: 代码规范 ;

var obj1 = {
  name: "obj1",
  foo: function () {
    console.log(this)
  }
}

var obj2 = {
  name: "obj2"
};

// obj2.bar = obj1.foo
// obj2.bar()  // 隐式调用 >> obj 2 

(obj2.bar = obj1.foo)() // 这种方式叫间接函数引用，属于独立函数调用，在浏览器中输出window,在node环境中输出{}