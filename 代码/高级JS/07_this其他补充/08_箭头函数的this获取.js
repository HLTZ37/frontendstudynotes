// 1.测试箭头函数中this指向
// 全部是 window
// var name = "why"

// var foo = () => {
//   console.log(this)
// }

// foo()    // 不会改变箭头函数中this的指向
// var obj = {foo: foo}
// obj.foo()  // 不会改变箭头函数中this的指向
// foo.call("abc") // 不会改变箭头函数中this的指向

// 2.应用场景
var obj = {
  data: [],
  getData: function () {
    /**
     * 发送网络请求, 将结果放到上面data属性中
     * 在箭头函数之前的解决方案
      var _this = this // 这里的this在使用下面的obj.getData()后，是指向obj的，
      如果没有这样的赋值，直接在settimeout中使用this.data = result 进行赋值时不能做到的，
      因为这里 的this是指向window（在浏览器环境下）

     */
    // 2.1  在没使用箭头函数时，为data赋值的方式
    // var _this = this 
    // setTimeout(function() {
    //   var result = ["abc", "cba", "nba"]
    //   _this.data = result
    // }, 2000);

    // 2.2 使用箭头函数之后
    setTimeout(() => {
      var result = ["abc", "cba", "nba"]
      this.data = result // 在使用obj.getData()后，这里二等this绑定的是obj
    }, 2000);
  }
}
obj.getData()