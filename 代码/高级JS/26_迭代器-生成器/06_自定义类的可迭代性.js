// class Person {

// }

// const p1 = new Person()
// const p2 = new Person()
// const p3 = new Person()

// 案例: 创建一个教室类, 创建出来的对象都是可迭代对象
class Classroom {
  constructor(address, name, students) {
    this.address = address
    this.name = name
    this.students = students
  }

  entry(newStudent) {
    this.students.push(newStudent)
  }

  [Symbol.iterator]() {
    let index = 0
    return {
      next: () => {
        if (index < this.students.length) {
          return {
            done: false,
            value: this.students[index++]
          }
        } else {
          return {
            done: true,
            value: undefined
          }
        }
      },
      return: () => {
        console.log("迭代器提前终止了~")
        return {
          done: true,
          value: undefined
        }
      }
    }
  }
}

const classroom = new Classroom("3幢5楼205", "计算机教室", ["james", "kobe", "curry", "why"])
classroom.entry("lilei")

for (const stu of classroom) {
  console.log(stu)
  if (stu === "why") break // 在类中的的定义的迭代器中，添加return 属性，即 吧 [Symbol.iterator](){next:function(){}, return:fucntion(){}}，就可以终止自定义类中的迭代
  // 其实，of 后面的变量是JS原生定义的可迭代对象，原生的可迭代对象内部已经实现了迭代器，即相关的next().return 等属性
}

function Person() {

}

Person.prototype[Symbol.iterator] = function () {

}