function* foo() {
  console.log("代码开始执行~")

  const value1 = 100
  try {
    yield value1
  } catch (error) {
    console.log("捕获到异常情况:", error)

    yield "abc" // 可以正常执行
  }

  console.log("第二段代码继续执行")
  const value2 = 200
  yield value2

  console.log("代码执行结束~")
}

const generator = foo()

const result = generator.next()
generator.throw("error message") // 执行throw() 时，第一段代码的yield 会报错  ，还可以传递参数 ，参数传递给catch()中
//  异常的捕获主要用在 异常的传递

// if (result.value != 200) {
//   generator.throw("error message") // 执行throw() 时，第一段代码的yield 会报错  ，还可以传递参数 ，参数传递给catch()中
// }