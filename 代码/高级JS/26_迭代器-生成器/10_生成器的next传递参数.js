// next中的参数会通过上一段的代码执行yield 时的返回值
function* foo(num) {
  console.log("函数开始执行~")

  const value1 = 100 * num
  console.log("第一段代码:", value1)
  const n = yield value1 // next中的参数会通过上一段的代码执行yield 时的返回值 ，所以这里的n是下一次调用next时，在next()中传递的参数

  const value2 = 200 * n // 这里就可以使用n 了
  console.log("第二段代码:", value2)
  const count = yield value2

  const value3 = 300 * count
  console.log("第三段代码:", value3)
  yield value3

  console.log("函数执行结束~")
  return "123"
}

// 生成器上的next方法可以传递参数
const generator = foo(5)
// console.log(generator.next())
// // 第二段代码, 第二次调用next的时候执行的
// console.log(generator.next(10))   
// console.log(generator.next(25))