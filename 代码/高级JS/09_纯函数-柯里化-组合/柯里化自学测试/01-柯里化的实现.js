function selfCurrying(fn) {
  // console.log(arguments) // 这里的参数是 selfCurrying函数的参数，只有一个，也就是传递的    函数fn
  // console.log(fn.length); //  fn.length 是传递的函数的参数的个数,是在函数的外部想获得参数的个数的方式，;如果是想知道当前函数参数的个数可以在函数内部使用 arguments.length 

  function curried(...args1) {
    // console.log(args1);
    // console.log(arguments);

    if (args1.length >= fn.length) {
      return fn(...args1)
      // return fn.call(this, ...args1);
    } else {
      var curried2 = function (...args2) {
        var concatArgs = args1.concat(args2) // 加上原来的参数
        return curried(...concatArgs)
      }
      return curried2
    }
  }
  return curried
}

function sum(num1, num2, num3) {
  return num1 + num2 + num3;

}
var sumCurried = selfCurrying(sum);
console.log(sumCurried(1, 3, 5));
console.log(sumCurried(1, 3)(5));
console.log(sumCurried(1)(3)(5));