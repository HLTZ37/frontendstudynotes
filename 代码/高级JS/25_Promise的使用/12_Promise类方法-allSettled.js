// 创建多个Promise
const p1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(11111)
  }, 1000);
})

const p2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    reject(22222)
  }, 2000);
})

const p3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(33333)
  }, 3000);
})

// allSettled
Promise.allSettled([p1, p2, p3]).then(res => {
  console.log(res)
}).catch(err => {
  console.log(err)
})


// allsetteld 和all的区别：
// 1.all  当只要有一个是reject状态时，最后的返回结果都是reject 。所有的Promise都变成fulfilled时, 再拿到结果
// 2.allsetteled 不会返回reject，而是返回各个promise的状态 。 =》 所有的Promise都变成结果时不管是fufilled还是reject, 再拿到结果

// 返回的顺序和返回时间无关，只和all()或allsettled中数组中规定的参数的顺序有关