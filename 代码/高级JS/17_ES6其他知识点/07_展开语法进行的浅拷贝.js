const info = {
  name: "why",
  friend: {
    name: "kobe"
  }
}

const obj = {
  ...info,
  name: "coderwhy"
} // name: "coderwhy" 会将起前面的name属性的值覆盖掉
// console.log(obj)
obj.friend.name = "james"

console.log(info.friend.name)