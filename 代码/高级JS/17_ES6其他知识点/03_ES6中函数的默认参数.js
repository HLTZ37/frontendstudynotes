// ES5以及之前给参数默认值
/**
 * 缺点:
 *  1.写起来很麻烦, 并且代码的阅读性是比较差
 *  2.这种写法是有bug，比如无法识别 0  和 “”的情况，需要多做判断，可读性不够好
 */
// function foo(m, n) {
//   m = m || "aaa"
//   n = n || "bbb"

//   console.log(m, n)
// }

// 1.ES6可以给函数参数提供默认值
function foo(m = "aaa", n = "bbb") {
  console.log(m, n)
}

// foo()
foo(0, "")

// 2.对象参数和默认值以及解构
function printInfo({
  name,
  age
} = {
  name: "why",
  age: 18
}) {
  console.log(name, age)
}

printInfo({
  name: "kobe",
  age: 40
})

// 另外一种写法
function printInfo1({
  name = "why",
  age = 18
} = {}) {
  console.log(name, age)
}

printInfo1()

// 3.有默认值的形参最好放到最后
function bar(x, y, z = 30) {
  console.log(x, y, z)
}

// bar(10, 20)                        
bar(undefined, 10, 20)

// 4.有默认值的函数的length属性,
// 函数有个属性叫length,表示参数的个数，计算方法是从与默认值的的位置开始往前算，且有默认值的参数不算在length中，有默认值的参数后的参数也不算在length中、
//  因此一般将幽默任职的参数放在参数列表的最后面，如下
function baz(x, y, z, m, n = 30) {
  console.log(x, y, z, m, n)
}

console.log(baz.length) // 4，nb不算在length中