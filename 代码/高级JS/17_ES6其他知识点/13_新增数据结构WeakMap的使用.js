const obj = {
  name: "obj1"
}
// 1.WeakMap和Map的区别二:
const map = new Map()
map.set(obj, "aaa")

const weakMap = new WeakMap()
weakMap.set(obj, "aaa")

// 解释：
// 对象{name: "obj1"} 目前有两个引用，情况1下分别是：obj和map 或者 情况2下分别是：obj和 weakMap 
// map是由Map 创建的，是强引用，weakMap是WeakMap创建的，是弱引用，
// 1)所以如果将上面的对象obj 指向null，即obj = null, 那么在情况1下对象{name: "obj1"} 是不会会GC回收的，因为map 是强引用
// 2)所以如果将上面的对象obj 指向null，即obj = null, 那么在情况2下对象{name: "obj1"} 是会被GC回收，因为weakMap 是弱引用

// 2.区别一: 不能使用基本数据类型
// weakMap.set(1, "ccc")

// 3.常见方法
// get方法
console.log(weakMap.get(obj))

// has方法
console.log(weakMap.has(obj))

// delete方法
console.log(weakMap.delete(obj))
// WeakMap { <items unknown> }
console.log(weakMap)