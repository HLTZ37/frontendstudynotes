# 一、numpy笔记

## 0.使用numpy

```python
import numpy as np 
```

## 1.创建narray

语法：arr = np.array(data,[dtype=np.**,...]) ,参数用来指定大小，参数1是必填，其他根据需要选填

```python
>>> data = [i for i in range(5)]
>>> data
[0, 1, 2, 3, 4]
>>> data = np.array(data)
>>> data
array([0, 1, 2, 3, 4])   # 对象变成array
```

![image-20210820100908824](assets/numpy-pandas笔记.fig/image-20210820100908824.png)

## 2.narray的属性

```python
>>> data.ndim  # 维度
1
>>> data.shape # 形状 （row,column,...）
(5,)
>>> data.dtype # 数据类型
dtype('int32')
```

## 3.一些快速函数

### 1. 创建指定长度的全0或全1的数组

1.1 全0 => 语法：arr = np.zeros((参数1,[参数2，参数3,...]) ,参数用来指定大小，参数1是必填，其他根据需要选填

1.2 全1 => 语法：arr = np.ones((参数1,[参数2，参数3,...]) ,参数用来指定大小，参数1是必填，其他根据需要选填

```python

>>> np.zeros(10)
array([0., 0., 0., 0., 0., 0., 0., 0., 0., 0.])
>>> np.zeros((2,3))
array([[0., 0., 0.],
       [0., 0., 0.]])
# 2. np.empty((参数1,参数2，参数3，...))  # 创建一个随机数值的数组，因为np.empty返回全0的数组的想法是不安全的，所以返回的一般是为初始化的垃圾值

```

### 2. np.empty((参数1,参数2，参数3，...)) 

​	创建一个随机数值的数组，因为np.empty返回全0的数组的想法是不安全的，所以返回的一般是为初始化的垃圾值

### 3.np.arange(n) 

​	默认返回创建一个范围从[0,n)的一维数组

### 4.np.arange(n).reshape((参数1，参数2,...))   

​	返回范围从[0,n)，形状为(参数1，参数2，...)的数组

### 5.np.random.randn(参数1，参数2，...)

​	生成服从正态分布的随机数据，该数据的形状由参数指定，一个参数就是一维数据，n个参数就是n为数据，参数值表示轴数

```python
>>> np.random.randn(3)
array([-0.80252341, -0.28039723,  0.4148072 ])
>>> np.random.randn(3,4)
array([[-0.37475785, -1.82021448,  1.18949286,  1.65536983],
       [ 0.37862261, -0.82268778,  0.67908757,  0.69329442],
       [ 1.11597547,  0.92229768,  0.88317935,  0.18607273]])
>>> np.random.randn(2,3,4)
array([[[-0.29969948,  1.2348124 , -0.79997726, -0.65289109],
        [-0.68111012,  1.22627165,  0.24351486,  1.60817725],
        [ 0.05961102,  0.5738852 ,  0.1957773 ,  2.12219176]],

       [[-0.51896972, -1.08210206,  0.14590338,  0.05110449],
        [-0.19808926,  0.29561999,  0.3210849 , -0.47317337],
        [-0.11880655,  0.41996862, -1.34594402,  1.15841959]]])
```

### 6.np.random.normal（size=(参数1，参数2，...))

​	生成服从标准正态分布的随机数据，该数据的形状由参数指定，一个参数就是一维数据，n个参数就是n为数据，参数值表示轴数

![image-20210820150916213](assets/numpy-pandas笔记.fig/image-20210820150916213.png)

![image-20210820150859066](assets/numpy-pandas笔记.fig/image-20210820150859066.png)

### 6.一元快速函数



<img src="assets/numpy-pandas笔记.fig/image-20210820110956618.png" alt="image-20210820110956618" style="zoom:67%;" />

<img src="assets/numpy-pandas笔记.fig/image-20210820111441885.png" alt="image-20210820111441885" style="zoom:67%;" />

```python
>>> arr5 = np.arange(5)
>>> arr5
array([0, 1, 2, 3, 4])
>>> np.sqrt(arr5)  # 各个元素的平方根
array([0.        , 1.        , 1.41421356, 1.73205081, 2.        ])
>>> np.exp(arr5)  # 各个元素的指数
array([ 1.        ,  2.71828183,  7.3890561 , 20.08553692, 54.59815003])

```

### 7.二元快速函数

<img src="assets/numpy-pandas笔记.fig/image-20210820111521494.png" alt="image-20210820111521494" style="zoom:67%;" />

```python
>>> arr5 = np.arange(5)
array([0, 1, 2, 3, 4])
>>> arr6=np.arange(5,10)
array([5, 6, 7, 8, 9])
>>> np.add(arr5,arr6)  # 二元快速函数
array([ 5,  7,  9, 11, 13])
>>
```



## 4.数组和标量之间的运算

```python
>>> arr = np.array([[1,2,3],[4,5,6]])
>>> arr*arr
array([[ 1,  4,  9],
       [16, 25, 36]])
>>> arr-arr
array([[0, 0, 0],
       [0, 0, 0]])
>>> arr/arr
array([[1., 1., 1.],
       [1., 1., 1.]])
>>> arr+arr
array([[ 2,  4,  6],
       [ 8, 10, 12]])
>>> 1/arr
array([[1.        , 0.5       , 0.33333333],
       [0.25      , 0.2       , 0.16666667]])
>>>
```

## 5.基本的切片索引

### 1.一维的常用切片索引

```python
>>> arr1 = np.arange(10)
array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
>>> arr1[3:6]  # 根据切片返回的范围：[start:end)
array([3, 4, 5])
>>> arr1[6:9]=55  # 会将[start:end)内的元素均赋值为55
array([ 0,  1,  2,  3,  4,  5, 55, 55, 55,  9])
>>> arr1[9]  # 普通索引
9
>>> arr1[:]  # ：号前后没有指定数字，默认返回全部
array([ 0,  1,  2,  3,  4,  5, 55, 55, 55,  9])


```

### 2.二维的切片索引

​		**强调：切片的返回返回范围的左闭又开区间的，即[1:5]=>意思是[start,end),即下标索引为，1,2,3,4 对应的数据**

```python
>>> arr2d=np.array([[1,2,3],[4,5,6],[7,8,9]])
>>> arr2d
array([[1, 2, 3],
       [4, 5, 6],
       [7, 8, 9]])
>>> arr2d[:2]
array([[1, 2, 3],
       [4, 5, 6]])
>>> arr2d[:2,:2]
array([[1, 2],
       [4, 5]])
>>> arr2d[:,:1]
array([[1],
       [4],
       [7]])
>>> arr2d[:,:1]=0  # 对切片的赋值也会扩散到整个切片选区
>>> arr2d
array([[0, 2, 3],
       [0, 5, 6],
       [0, 8, 9]])
>>>
```

## 6.数组转置和轴对换

```python
>>> arr4 = np.arange(12).reshape((3,4))
>>> arr4
array([[ 0,  1,  2,  3],
       [ 4,  5,  6,  7],
       [ 8,  9, 10, 11]])
>>> arr4.T	# 矩阵的转置
array([[ 0,  4,  8],
       [ 1,  5,  9],
       [ 2,  6, 10],
       [ 3,  7, 11]])
>>> arr4.np.dot(arr4.T,arr4)  # np.dot()计算矩阵的内积
>>> np.dot(arr4.T,arr4)
array([[ 80,  92, 104, 116],
       [ 92, 107, 122, 137],
       [104, 122, 140, 158],
       [116, 137, 158, 179]])
>>>
```

<img src="assets/numpy-pandas笔记.fig/image-20210820143519887.png" alt="image-20210820143519887" style="zoom:67%;" />



## 7.数学和统计方法

### 1.基本的统计方法

![image-20210820143710895](assets/numpy-pandas笔记.fig/image-20210820143710895.png)

![image-20210820143719230](assets/numpy-pandas笔记.fig/image-20210820143719230.png)

```python
>>> arr = np.arange(1,13).reshape((4,3))
array([[ 1,  2,  3],
       [ 4,  5,  6],
       [ 7,  8,  9],
       [10, 11, 12]])
>>> arr.mean()
6.5
>>> arr.sum()
78
>>> arr.sum(axis=1)   # 轴向为1的方向，也就是行方向，二维的时候，轴的范围是[0,1],只有两个值
array([ 6, 15, 24, 33])
>>> arr.sum(axis=0)  # 轴向为0的方向，也就是列方向，二维的时候，轴的范围是[0,1],只有两个值，下面函数中的参数同理
array([22, 26, 30])
>>> arr.cumsum()   # 没有指定轴时，延展为一维
array([ 1,  3,  6, 10, 15, 21, 28, 36, 45, 55, 66, 78], dtype=int32)
>>> arr.cumsum(0)
array([[ 1,  2,  3],
       [ 5,  7,  9],
       [12, 15, 18],
       [22, 26, 30]], dtype=int32)
>>> arr.cumprod(0)
array([[   1,    2,    3],
       [   4,   10,   18],
       [  28,   80,  162],
       [ 280,  880, 1944]], dtype=int32)
>>> arr.cumprod(1)
array([[   1,    2,    6],
       [   4,   20,  120],
       [   7,   56,  504],
       [  10,  110, 1320]], dtype=int32)
>>>
```

### 2.排序：sort()就地排序

```python
>>> b = [23,54,32,44,55,67]
>>> b = np.array(b)
array([23, 54, 32, 44, 55, 67])
>>> b.sort()
>>> b
array([23, 32, 44, 54, 55, 67])
>>>
# 根据轴向排列 

>>> arr=np.array([[2,5,8,3,2],[5,12,44,54,23],[12,21,23,41,16]])
>>> arr
array([[ 2,  5,  8,  3,  2],
       [ 5, 12, 44, 54, 23],
       [12, 21, 23, 41, 16]])
>>> arr.sort(1)  # 按行轴排序
>>> arr
array([[ 2,  2,  3,  5,  8],
       [ 5, 12, 23, 44, 54],
       [12, 16, 21, 23, 41]])
>>> arr.sort(0)  # 按列轴排序
>>> arr
array([[ 2,  2,  3,  5,  8],
       [ 5, 12, 21, 23, 41],
       [12, 16, 23, 44, 54]])
>>> arr.sort()  # 当sort中不传递参数时，默认按照行轴排序，即axis=1
>>> arr
array([[ 2,  2,  3,  5,  8],
       [ 5, 12, 23, 44, 54],
       [12, 16, 21, 23, 41]])
```

### 3.唯一化和其他常用集合逻辑

```python
# 返回数组中的唯一值，也就是常见的去重，看数组中个都有那些类型的数据，除了可以唯一化字符/字符串，数值型的也可以
>>> name = np.array(['a','b','a','c','d','c'])
>>> name
array(['a', 'b', 'a', 'c', 'd', 'c'], dtype='<U1')
>>> np.unique(name)
array(['a', 'b', 'c', 'd'], dtype='<U1'
```

![image-20210820145928923](assets/numpy-pandas笔记.fig/image-20210820145928923.png)

## 8.用于数组的文件输入输出

np.save()和np.load()是读取磁盘数组的两个主要函数，默认情况下，数组一无压缩的原始二进制格式保存在扩展名为.npy的文件中。

如果文件路径的末尾没有扩展名.npy，则改扩展名会自动加上

```python
# 语法：np.save(filname,array)    filname 指的文件名，可以是路径名/文件名的形式，array，指的是要保存的数组，没有指定路径名，会默认保存在当前文件下
>>> name
array(['a', 'b', 'a', 'c', 'd', 'c'], dtype='<U1')
>>> np.save("C:/Users/admin/Desktop/np_save_test",name)
>>> np.load("C:/Users/admin/Desktop/np_save_test.npy")
array(['a', 'b', 'a', 'c', 'd', 'c'], dtype='<U1')
```

**还可以加载txt文件**：np.loadtxt

```python
>>> ff = np.loadtxt("C:/Users/admin/Desktop/loadtxtTest.txt")  # 需要注意的是元素之间不能以其他符号隔开，比如‘,’，不然无法转转成功
														# 报错 ValueError: could not convert string to float: '2,'
>>> ff
array([[ 2.,  2.,  3.,  5.,  8.],
       [ 5., 12., 23., 44., 54.],
       [12., 16., 21., 23., 41.]])
```

# 二、pandas

## 0 .pandas的使用

一般会使用到pandas的 Series 和DataFrame

```python
import pandas as pd
from pandas import Series
from pandas import DataFrame
```

## 1.pandas 的基本操作

### 1.1 Series

#### 1.1.1 创建

- **Series的字符串表示形式为：索引在左，值在右。当没有指定索引是，会自动创建一个0到N-1的整型索引**

```python
# 1.创建
>>> import pandas as pd
>>> obj = pd.Series([1,2,3,4])
0    1
1    2
2    3
3    4
dtype: int64
 # 或
>>> from pandas import Series
>>> obj = Series([1,2,3,4])
# 2. 通过 Series 的 value 和 index 属性获取其数组
>>> obj.values
array([1, 2, 3, 4], dtype=int64)
>>> obj.index
RangeIndex(start=0, stop=4, step=1)
# 3. 为数据指定索引
>>> obj2 = Series(np.arange(4),index=['a','b','c','d'])
# 还可以写成  index_series=['a','b','c','d'] obj2 = Series(np.arange(4),index=index_seriex)
>>> obj2
a    0
b    1
c    2
d    3
dtype: int32
# 4. 当数组是字典的形式，也可以直接通过这个字典创建Series
>>> ob = {'a':12,'b':13,'c':15,'d':23}
>>> ob = Series(ob)
>>> ob
a    12
b    13
c    15
d    23
dtype: int64
    
```

#### 1.1.2 一些基本访问操作

```python
#  访问创建好的Series对象
>>> obj2['a']
0
>>> obj[2]
3
# 需要注意的过程是，下面这些操作不改变原数组的值，只是开辟了一个新的变量来存储操作的值
>>> obj2[obj2>1] 	# ##
c    2
d    3
dtype: int32
>>> obj2*2  # ##
a    0
b    2
c    4
d    6
dtype: int32
>>> np.exp(obj2) # ##
a     1.000000
b     2.718282
c     7.389056
d    20.085537
dtype: float64
```

### 1.2  DataFrame

**DataFrame是一个表格型数据结构，他含义与一组有序的列，每个列可以是不同的值类型（数值，字符串，布尔值），DataFrame既有行索引又有列索引**

#### 1.2.1 创建

```python
# 1
>>> dates = pd.date_range('20210101',periods=6)
>>> dates
DatetimeIndex(['2021-01-01', '2021-01-02', '2021-01-03', '2021-01-04',
               '2021-01-05', '2021-01-06'],
              dtype='datetime64[ns]', freq='D')
>>> df = pd.DataFrame(np.random.randn(6,4),index=dates,columns=list("ABCD"))
>>> df
                   A         B         C         D
2021-01-01 -0.157468  1.904915  0.287935  1.247956
2021-01-02  1.667547  0.512583 -0.972624  0.323247
2021-01-03 -1.185240 -0.320585  0.757794  0.972576
2021-01-04  0.946071  0.446559 -0.580150  0.982047
2021-01-05 -0.546686  0.123196  1.479660 -0.396528
2021-01-06  1.342440 -0.355782 -2.082392  1.370565
#	2.用 Series 字典对象生成 DataFrame:
In [9]: df2 = pd.DataFrame({'A': 1.,
   ...:                     'B': pd.Timestamp('20130102'),
   ...:                     'C': pd.Series(1, index=list(range(4)), dtype='float32'),
   ...:                     'D': np.array([3] * 4, dtype='int32'),
   ...:                     'E': pd.Categorical(["test", "train", "test", "train"]),
   ...:                     'F': 'foo'})
   ...: 

In [10]: df2
Out[10]: 
     A          B    C  D      E    F
0  1.0 2013-01-02  1.0  3   test  foo
1  1.0 2013-01-02  1.0  3  train  foo
2  1.0 2013-01-02  1.0  3   test  foo
3  1.0 2013-01-02  1.0  3  train  foo




```

