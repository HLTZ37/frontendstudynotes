# vscode-webpack打包出错总结

# 1.C:\Users\admin\AppData\Roaming\npm\webpack.ps1，因为在此系统上禁止运行脚本。

执行：webpack .\src\index.js .\dist\bundle.js ，报错

**解决：**

链接：https://blog.csdn.net/wmiaopas/article/details/102877964

1.首先以管理员身份打开windows PowShell终端

![image-20210714112324912](assets/vscode-webpack打包出错总结.fig/image-20210714112324912.png)

2.此时在终端执行，如果弹出提示则选择Y，再执行如下：

```
get-help set-executionpolicy
```

![image-20210714112502990](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20210714112502990.png)

然后再执行：

```
set-executionpolicy remotesigned
```

如果弹出提示则选择Y， 然后就可以进行打包了

# 2.Webpack 和 css ,style对应loader 的版本问题

原问题：安装的webpack 是3.6.0, 因为没有指定安装的style-loader和css-loader的版本，直接使用指令 npm install style-loader/css-loader  --save-dev 安装的是最新的版本，结果出现警告：

```
npm WARN css-loader@5.2.7 requires a peer of webpack@^4.27.0 || ^5.0.0 but none is installed. You must install peer dependencies yourself.
npm WARN style-loader@3.1.0 requires a peer of webpack@^5.0.0 but none is installed. You must install peer dependencies yourself.
npm WARN meetwebpack@1.0.0 No repository field.
```

提示webpack和css/style  loader 的版本不对应，打包执行命令：npm  run  build 时报错

![image-20210714204410368](assets/vscode-webpack打包出错总结.fig/image-20210714204410368.png)

**解决方法：**webpack3.6.0版本与loaders，plugins版本的兼容性问题解决，即便已经安装的高版本，使用指定新的安装版本就会覆盖替换掉原来的,如：

 npm install css-loader@3.0.0  --save-dev 

![image-20210714205009645](C:\Users\admin\AppData\Roaming\Typora\typora-user-images\image-20210714205009645.png)

# 3.使用 webpack 打包，图片地址变成了 [object object]又或者图片没有报错却加载不出来

**链接：https://blog.csdn.net/weixin_50680057/article/details/109246024**

# 4.webpack结合vue 初始报错（在running-only）



![image-20210715211509771](assets/vscode-webpack打包出错总结.fig/image-20210715211509771.png)

因为 vue在构建最终的发布版本的时候 ，构建了两个版本，分别是：

​	a.runtime-only:代码中不可以有任何的template,通过vue实例挂载在页面的上的实例app也是一个template,(也就是在实例中，不能有template 的关键字)

​	b.runtime-complier:代码中可以有template,因为有template可被编译的工具complier

![image-20210715212504080](assets/vscode-webpack打包出错总结.fig/image-20210715212504080.png)

**解决办法：**

![img](E:\有道笔记_hltz37\hltz37@126.com\48a9b1c0a6de400a933ea0a09082e1e7\clipboard.png)

```
  resolve: {
​    // alias 别名
​    alias: {
​      'vue$': 'vue/dist/vue.esm.js',   // 'vue$'表示以vue 结尾的文件夹，具体在项目文件下的node_modules文件中的vue文件夹，默认使用的是vue/dist文件										下面的vue.runtime.js 文件，现在更改成vue.esm.js文件，这样就可以编译template 了
​    }
  }
```

5.webpack-dev-server : 无法将“webpack-dev-server”项识别为 cmdlet、函数、脚本文件或可运行程序的名称。请检查名称的拼写，如果包括路径，请确保路径正确，然后再试一次。
所在位置 行:1 字符: 1

![image-20210719203922928](assets/vscode-webpack打包出错总结.fig/image-20210719203922928.png)

或者报错：

![image-20210719203336963](assets/vscode-webpack打包出错总结.fig/image-20210719203336963.png)

**原因：是因为webpack-server只是在局部安装，在终端中（执行的都是全局的）在全局找不到，所以才会报错**

**解决方法1：**

​	![image-20210719204409346](assets/vscode-webpack打包出错总结.fig/image-20210719204409346.png)

  "dev":"webpack-dev-server"   // 表示在开发时执行脚本 webpack-dev-server。然后在终端中执行的命令是npm  run dev （后话：其实同理"build":"webpack"  // 表示执行开发时 若使用 npm  run build ,这句实际执行的是webpack脚本，都是通过package.json 文件做配置的）

**注意：需要提前把index.html入口文件 和dist 文件绑定在一起，不然 使用 npm  run  dev 时找不到入口文件，页面显示的时候可能就只是一个路径文件夹**





​	

