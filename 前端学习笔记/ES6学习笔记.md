# 0."use strict" 

"use strict" 指令在 JavaScript 1.8.5 (ECMAScript5) 中新增。

它不是一条语句，但是是一个字面量表达式，在 JavaScript 旧版本中会被忽略。

"use strict" 的目的是指定代码在严格条件下执行。
**如：**

- 不允许使用未声明的变量；
- 不允许删除变量或对象；
- 不允许删除函数；
- 不允许变量重名:
- 不允许使用八进制
- 不允许使用转义字符；
- 不允许对只读属性赋值:
- 由于一些安全原因，在作用域 eval() 创建的变量不能被调用；
- 禁止this关键字指向全局对象。

# 1.ES6简介

JavaScript 的创造者 `Netscape` 公司，决定将 JavaScript 提交给标准化组织 `ECMA`，希望这种语言能够成为国际标准。

ES6 既是一个历史名词，也是一个泛指，含义是 5.1 版以后的 JavaScript 的下一代标准，涵盖了 ES2015、ES2016、ES2017 等等，而 `ES2015` 则是`正式名称`，特指该年发布的正式版本的语言标准。本书中提到 ES6 的地方，一般是指 ES2015 标准。

# 2.ES6 let 与 const

## 2.1. let 命令

`ES6` 新增了`let`命令，用来声明变量。它的用法类似于var，但是所声明的变量，只在let命令所在的代码块内有效。

优点：

- 不存在变量提升。var命令会发生“变量提升”现象，即变量可以在声明之前使用，值为undefined。let命令改变了语法行为，它所声明的变量一定要在声明后使用，否则报错。报错ReferenceError
- 暂时性死区。只要块级作用域内存在let命令，它所声明的变量就“绑定”（binding）这个区域，不再受外部的影响。
- 不允许重复声明。

## 2.2. 块级作用域

**ES6 的块级作用域必须有大括号，如果没有大括号，JavaScript 引擎就认为不存在块级作用域。**

for循环还有一个特别之处，就是设置循环变量的那部分是一个父作用域，而循环体内部是一个单独的子作用域。

这表明函数内部的变量i与循环变量i不在同一个作用域，有各自单独的作用域

## 2.3. const 命令

`const`声明一个只读的`常量`。**一旦声明，常量的值就不能改变**。const声明的变量不得改变值，这意味着，**const一旦声明变量，就必须立即`初始化`**，不能留到以后赋值。

```js
// 只声明不赋值，就会报错:SyntaxError: Missing initializer in const declaration
```

const命令声明的常量也是不提升，同样存在暂时性死区，只能在声明的`位置后面`使用。

const声明的常量，也与let一样不可重复声明。

**`ES5` 只有两种声明变量的方法：`var`命令和`function`命令。`ES6` 除了添加`let`和`const`命令，后面章节还会提到，另外两种声明变量的方法：import命令和class命令。所以，ES6 一共有 6 种声明变量的方法。**

| 声明方式 | 变量提升 | 暂时性死区 | 重复声明 | 初始值 | 最小作用域 |
| :------: | :------: | :--------: | :------: | :----: | :--------: |
|   var    |   允许   |   不存在   |   允许   | 不需要 | 函数作用域 |
|   let    |  不允许  |    存在    |  不允许  | 不需要 | 块级作用域 |
|  const   |  不允许  |    存在    |  不允许  |  需要  | 块级作用域 |

# 3.箭头函数

## 3.1 箭头函数规则

1. 箭头函数只能用赋值式写法，不能用声明式写法；
2. 如果参数只有一个，可以不加括号，如果没有参数或者参数多于一个就需要加括号；
3. 如果函数体只有一句话，可以不用花括号；有花括号就要有return，函数体的花括号与return同在；

## 3.2 箭头函数注意点

1. 函数体内的 this 对象，就是**定义时所在的对象，必须通过查找作用域链来决定其值。**而不是使用时所在的对象。

   如果箭头函数被非箭头函数包含，则 this 绑定的是最近一层非箭头函数的 this，否则，this 为 undefined。

2. 不可以当作构造函数，也就是说，不可以使用 new 命令，否则会抛出一个错误。

3. 不可以使用 arguments 对象，该对象在函数体内不存在。如果要用，可以用 rest 参数代替。

4. 不可以使用 yield 命令，因此箭头函数不能用作 Generator 函数。

5. 不能用 `call()`、`bind()`、`apply()` 修改里面的this

## 3.3 普通函数this（this指向）

==**普通函数 this 永远指向最后调用它的那个对象，**==

==**普通函数 this 永远指向最后调用它的那个对象，**==

==**普通函数 this 永远指向最后调用它的那个对象**。==

```js
var name = "windowsName";
var a = {
    // name: "Cherry",
    fn : function () {
        console.log(this.name);      // undefined
    }
}
window.a.fn();
```

**this 的使用场景：**

- **函数调用模式，**当一个函数直接作为函数来调用时，this 指向全局对象。如，window.setTimeout()和window.setInterval()
- **方法调用模式，**如果一个函数作为一个对象的方法来调用时，this 指向这个对象。
- **构造器调用模式，**如果一个函数用 new 调用时，函数执行前会新创建一个对象，this 指向这个新创建的对象。
- **apply 、call 和 bind 调用模式，**这三个方法都可以显示的指定调用函数的 this 指向。第一个参数如果是undefined，那么this指针指向的是window对象。

**调用模式优先级：构造器调用模式 > apply 、 call 和 bind 调用模式 > 方法调用模式 > 函数调用模式**

## 3.4 箭头函数this

1. 箭头函数中没有 this 绑定，对象的this就是**定义时所在的对象**，而不是使用时所在的对象。必须通过查找作用域链来决定其值，如果箭头函数被非箭头函数包含，则 **this 绑定的是最近一层非箭头函数的 this**，否则，this 为 undefined。

   前面说到 window.setTimeout() 中函数里的this默认是window，可以通过在setTimeout中写箭头函数，使setTimeout的this和外层this（箭头函数定义时的外层this）保持一致

2. 不能用 call()、bind()、apply() 方法修改里面的this

```js
    var name = "windowsName";
    var a = {
        name : "Cherry",
        func1: function () {
            console.log(this.name)     
        },
        func2: function () {
            setTimeout( function () {
                this.func1()
            },100);
        },
        func3: function () {
            setTimeout( () => {
                this.func1()
            },100);
        }
    };

    a.func2()     // this.func1 is not a function
 	a.func3()     // Cherry
```

## 3.5 怎么改变 this 的指向

- 使用 ES6 的箭头函数
- 在函数内部使用 `_this = this`
- 使用 `apply`、`call`、`bind`
- new 实例化一个对象

## 3.6 应用

```js
let a = (x) => x
let b = (x) => {x;}
let c = (x) => {{x;}}
let d = (x) => ({x;})
console.log(a(1), b(1), c(1), d(1));  // 输出：1 undefined undefined {x:1}
```

# *.数组

