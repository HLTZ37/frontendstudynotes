# 一、项目介绍

基于Vuejs 开发的项目实战

# 二、目录划分

![image-20210812104550308](assets/Vuejs实战总结.fig/image-20210812104550308.png)

# 三、实践随记

## 1. v-if 和v-show的区别

**`v-if` 是“真正”的条件渲染，**因为它会确保在切换过程中条件块内的事件监听器和子组件适当地被销毁和重建。

`v-if` 也是**惰性的**：该指令仅在组件为`true`时才渲染。 如果为`false`，则该组件在**DOM**中不存在。

相比之下，`v-show` 就简单得多——不管初始条件是什么，**元素总是会被渲染，并且只是简单地基于 CSS 进行切换。**

**带有 `v-show` 的元素始终会被渲染并保留在 DOM 中。`v-show` 只是简单地切换元素的 CSS property `display`。**

一般来说，`v-if` 有更高的切换开销，而 `v-show` 有更高的初始渲染开销。因此，如果需要非常频繁地切换，则使用 `v-show` 较好；如果在运行时条件很少改变，则使用 `v-if` 较好。

v-if 有真正的条件渲染，即若为true则显示这个，反之显示另一个。v-show则只有true的时候显示，false的时候不显示,没有else的选择

## 2.关于在vue中使用echarts时碰到的问题: Error Initialize failed invalid dom

![image-20210909100508893](assets/Vuejs实战总结.fig/image-20210909100508893.png)

 原因是dom还没挂载完成，导致报错，这里有几个处理办法：

   **1.1 这里不要用created（用mounted），created这时候还只是创建了实例，但模板还没挂载完成
**   1.2 用this.$nextTick(()=>{}) (这个回调函数会在数据挂载更新完之后执行，所以可行)
   1.3 使用Promise

https://blog.csdn.net/smalCat/article/details/89091966

https://blog.csdn.net/liucai1018/article/details/113744623?utm_medium=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.no_search_link&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-2%7Edefault%7ECTRLIST%7Edefault-1.no_search_link

## 3.Vue中的页面跳转方式总结

### 1.使用标签组件 router-link

在需要跳转的地方可以添加组件<router-link>，具体使用如下：

```vue
<!-- 一、其中 to 用于指定跳转的路径传递参数 
    1.path指的是在router下index.js中 创建组件路由对象中的path指定的属性值，即
         {
            path: "/threeSessionDataSumm",
            name: "threeSessionDataSumm",
            component: threeSessionDataSumm
          },
    2.query 指定传递的参数
	二、 如何或区域
-->
<router-link  :to="{path:'/'   query: { title: this.title, type: 'aaa'}" tag="button" class="backBtn" >返回</router-link>
<!-- 二、如何在跳转后页面获取前一个页面传递过来的参数？
	方法：在跳转后页面中的created(){}中获取，如下所示：
      created() {
        this.party = this.$route.query.party;
      },
	 其中this.$route指的是当前页面路由
-->

```

### 2.点击响应函数中使用 this.$router.push/replace()函数做跳转

这种方法可以使用点击事件响应的方式，具有使用如下：、

```js html
// html ：
<button class="switchPage"  @click="backToDataAnalysis()">返回</button>
// js 或是在.vue中的script标签的methods中
  methods: {
    backToDataAnalysis() {
      console.log("backToDataAnalysis");
      this.$router.push({
        path: "/dataAnalysis",
      });
    },
  },
// 1.这里 this.$router.push()中的参数可以是对象：{path:"跳转的路径，路径也是在router的index.js的指定的组件路由的path属性指定的值，同上"，query:{name:"aaa",...}},query 用来指定条指定跳转传递的参数
// 2.这里 this.$router.push()跳转后，还可以借助浏览器自带的返回按钮返回上一页面，但是有些应用场景不允许跳转，如支付页面等，这时可以使用 this.$router.replace()代替
      
```

## 4.javascript如何判断字符串中是否包含某个字符串

**方法一: indexOf()**

indexOf() 方法可返回**某个指定的字符串值在字符串中首次出现的位置**。如果要检索的字符串值没有出现，则该方法返回 -1。

```js
var str = "123";
console.log(str.indexOf("3") != -1 ); // true
```

**方法二: search()**

```javascript
var str = "123";
console.log(str.search("3") != -1 ); // true
```

search() 方法用于检索字符串中指定的子字符串，或检索与正则表达式相匹配的子字符串。如果没有找到任何匹配的子串，则返回 -1。

**方法三:match()**

```javascript
var str = "123";
var reg = RegExp(/3/);
if(str.match(reg)){
    // 包含       
}
```

match() 方法可在字符串内检索指定的值，或找到一个或多个正则表达式的匹配。

**方法四:test()**

```javascript
var str = "123";
var reg = RegExp(/3/);
console.log(reg.test(str)); // true
```

test() 方法用于检索字符串中指定的值。返回 true 或 false。

**方法五:exec()**

```js
var str = "123";
var reg = RegExp(/3/);
if(reg.exec(str)){
    // 包含       
}
```

exec() 方法用于检索字符串中的正则表达式的匹配。返回一个数组，其中存放匹配的结果。如果未找到匹配，则返回值为 null。

## 5.如何将tabbbar中的子项和试图页面联系在一起？

![image-20211019195900966](assets/Vuejs实战总结.fig/image-20211019195900966.png)

**准备：**

（1）创建每个tabbar子项对应的视图页面（或者组件）

（2）创建标签<router-view></router-view>与tabbar对应的dom 同等级并列

**方法：**

（1）给tabbar中的每一个子项**添加监听事件**

（2）同时采用父组件给子组件传递参数的方式，也就是tabbar给tabbar-item组**件传递参数：path**，这个path代表该tabbar子项对应的视图页面（组件）的path，也就是在router下index.js中定义的路由中的path

（3）监听函数要做的事：获取当前被点击的tabbar子项传递的参数path，使用路由router，视图页面显示该path指定的组件，即thie.$router.replace(this.path)

这样<router-view>会显示router 指定的组件

**额外：**

至于视图的位置和大小，可以使用绝对定位，如：{position:absolute;top:0;right:0;height:0;bottom：tabbar的高度}

## 6.tabbar中默认显示首页的方法

问题描述：tabbar的子项中，浏览器变成根目录，下面的tabbar子项中全部成没选中状态，但是应该默认第一个或者首页被选中，tabbar子项成高亮状态

此时的router index.js文件的状态：（如图所示，tabbar第一个子项没有高亮显示，按理来说，当用户进来时，应该有一个默认高亮显示）

![image-20211019205607289](assets/Vuejs实战总结.fig/image-20211019205607289.png)

**方法：**

将默认的路由指向，使用**redirect**关键字指向别的路由，方法如图所示：

![image-20211019212901597](assets/Vuejs实战总结.fig/image-20211019212901597.png)



## 7. 在vue中添加jquery

第一步：安装jquery 

```js
npm  install jquery --save
```

第二步：配置文件

1. main.js中引入

   ```js
   import $ from "jquery"
   window.jquery = $;
   window.$ = $;
   ```

2. 在webpack.bae.js引入

   注意：vuecli2和vuecli3搭建的文件结构不一样，vuecli2下，该文件位于根目录下的build文件下， vuelci3下，可能在vue.config.js中配置（这半句是猜的，没有考证，后面查啊，记得重启项目）

   ```js
   const webpack = require('webpack')  
   plugins: [
       new webpack.ProvidePlugin({
         $: "jquery",
         jQuery: "jquery",
         "windows.jQuery": "jquery"
       })
     ],
   ```

   
   
   ## 8.同源跨域问题
   
   ```js
   Access to XMLHttpRequest at 'http://localhost:8082/login' (redirected from 'http://10.3.116.11:8081/getProjectInfo') from origin 'http://localhost:8082' has been blocked by CORS policy: The value of the 'Access-Control-Allow-Origin' header in the response must not be the wildcard '*' when the request's credentials mode is 'include'. The credentials mode of requests initiated by the XMLHttpRequest is controlled by the withCredentials attribute.
   ```
   
   

