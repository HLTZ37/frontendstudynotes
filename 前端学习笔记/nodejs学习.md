# 一、Nodejs基础

## 1.nodejs

![image-20210804101513280](assets/nodejs学习.fig/image-20210804101513280.png)

![image-20210804095512053](assets/nodejs学习.fig/image-20210804095512053.png)

## 2.浏览器

### 2.1 浏览器内核

![image-20210804101850840](assets/nodejs学习.fig/image-20210804101850840.png)

### 2.2 浏览器渲染过程

![image-20210804102403042](assets/nodejs学习.fig/image-20210804102403042.png)

### 2.3 JS 引擎

![image-20210804102426808](assets/nodejs学习.fig/image-20210804102426808-16496855291913.png)

![image-20210804102925602](assets/nodejs学习.fig/image-20210804102925602.png)

![image-20210804104002050](assets/nodejs学习.fig/image-20210804104002050.png)

### 2.4 webkit内核

![image-20210804102657990](assets/nodejs学习.fig/image-20210804102657990.png)

### 2.5 node js 架构

![image-20210804105201395](assets/nodejs学习.fig/image-20210804105201395.png)

![image-20210804104957890](assets/nodejs学习.fig/image-20210804104957890.png)

![image-20220108203015986](assets/nodejs学习.fig/image-20220108203015986.png)

## 3.Node的版本管理

![image-20220108212315064](assets/nodejs学习.fig/image-20220108212315064.png)

## 4. Nodel 的REPL

![image-20220108212401649](assets/nodejs学习.fig/image-20220108212401649.png)

5. ## 给Node中传递参数

   ![image-20220108215626852](assets/nodejs学习.fig/image-20220108215626852.png)

   <img src="assets/nodejs学习.fig/image-20220108215331742.png" alt="image-20220108215331742" style="zoom: 67%;" />

   

## 3.Node中的全局对象

![image-20210805153800909](assets/nodejs学习.fig/image-20210805153800909.png)

![image-20210805154029044](assets/nodejs学习.fig/image-20210805154029044.png)

**Node中一个文件就是一个模块，文件之间的变量等之间是不能直接访问的。，文件创建时候，会伴随创建一个Module 对象，是Node用来导出文件使用的**

# 二、Node中javaScript 的模块化

## 1.什么是模块化及存在的问题

![image-20210805154837828](assets/nodejs学习.fig/image-20210805154837828.png)

![image-20210805155313497](assets/nodejs学习.fig/image-20210805155313497.png)

![image-20210805161053218](assets/nodejs学习.fig/image-20210805161053218.png)

## 2.commonJS 和Node的关系

![image-20210805161612462](assets/nodejs学习.fig/image-20210805161612462.png)

## 3.commonJS 实现的本质

> node中commonJS 的实现本质是实现**引用赋值**

![image-20210805163252375](assets/nodejs学习.fig/image-20210805163252375.png)

![image-20210805164618024](assets/nodejs学习.fig/image-20210805164618024.png)

![image-20210805201522776](assets/nodejs学习.fig/image-20210805201522776.png)

exports存在是因为：commonJS的规范中要求要有一个exports作为导出，默认情况下exports和Module.export是同一个地址（指向相同），但若后面Module.exports发生了改变（涌现的对象赋值），require则会以Module.export为准，指向Module.export指向的对象，而不是原来的的对象，但是exports任然指向的是原来的对象

**值拷贝和引用拷贝**：值拷贝是将值直接赋值，引用拷贝赋值的是地址（指针）

![image-20210805201150268](assets/nodejs学习.fig/image-20210805201150268.png)

![image-20220109182749200](assets/nodejs学习.fig/image-20220109182749200.png) 

![image-20210805202200250](assets/nodejs学习.fig/image-20210805202200250.png)

## 4.AMD介绍

require(),可以访问函数外部的数据

## 5 .CDM介绍



## 6.ES6

![image-20210805205514276](assets/nodejs学习.fig/image-20210805205514276.png)

![image-20210805205641518](assets/nodejs学习.fig/image-20210805205641518.png)

![image-20210805205809727](assets/nodejs学习.fig/image-20210805205809727.png)

![image-20210805210717734](assets/nodejs学习.fig/image-20210805210717734.png)



![image-20210805211837973](assets/nodejs学习.fig/image-20210805211837973.png)

## 7.Node常见的内置模块

### 7.1 path

![image-20210806095558009](assets/nodejs学习.fig/image-20210806095558009.png)

![image-20220111151232805](assets/nodejs学习.fig/image-20220111151232805.png)

```js
// 导入path 模块
const path = require("path");
// 其他常用方法 
// 1.获取路径的信息
const filepath = "/User/why/abc.txt";
console.log(path.dirname(filepath)); // 获取文件的父文件夹
console.log(path.basename(filepath)); // 获取文件名字
console.log(path.extname(filepath)); // 获取文件的扩展名

// 2. 路径的拼接 path.join()
const basePath = "/user/why";
const filename = "abc.txt";
const filepath1 = path.join(basePath, filename);
console.log(filepath1);

// 3. path.join()和path.resolve()的区别
// resolve 会判断境界的路径字符串中是否有 / 或者 ./, ../开头的路径，并补充完整，也就是解析 /，./， ../这个路径符号的完成路径 
const filepath2 = path.resolve(basePath, filename);
console.log(filepath2);
```



### 7.2 fs（file sysytem）

![image-20210806101057948](assets/nodejs学习.fig/image-20210806101057948.png)

![image-20210806101230071](assets/nodejs学习.fig/image-20210806101230071.png)

```js
// 导入模块
const fs = require('fs')

const filepath = "E:/nodeJsStudy/03-常见的内置模块/02-内置模块fs/abc.txt"; // 写成相对路径不起作用。不知道为啥
// 方式一：同步操作
const info = fs.statSync(filepath);
console.log("后续需要执行的代码");
console.log(info);

// 方式二： 异步操作
fs.stat(filepath, (info, err) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log(info);
});
console.log("后续需要执行的代码");

// 方式三：promise的操作方式
fs.promises.stat(filepath).then(info => {
  console.log(info);
}).catch(err => {
  console.log(err);
})
console.log("后续需要执行的代码");
```

#### 7.2.1 文件的读写

![image-20220111204800770](assets/nodejs学习.fig/image-20220111204800770.png)

![image-20220111204827348](assets/nodejs学习.fig/image-20220111204827348.png)

#### 7.2.2  文件描述符

![image-20220222174010266](assets/nodejs学习.fig/image-20220222174010266.png)

![image-20220222173905282](assets/nodejs学习.fig/image-20220222173905282.png)



### 7.3 events 模块

![image-20220114100157667](assets/nodejs学习.fig/image-20220114100157667.png)

## 8.CommonJS 和esModule的交互

![image-20220222095559808](assets/nodejs学习.fig/image-20220222095559808.png)



# 三、包管理工具 npm

## 0.包管理工具

![image-20220223093827144](assets/nodejs学习.fig/image-20220223093827144.png)



## 1.一些常见的属性

### 1.1 name,version，mian

![image-20220114153647235](assets/nodejs学习.fig/image-20220114153647235.png)

<img src="assets/nodejs学习.fig/image-20220114153627412.png" alt="image-20220114153627412" style="zoom: 80%;" />

### 1.2 script属性

```js
// 以下四个属性不需要的 在执行时 可以使用 不加run 关键字执行，比如：npm  start (原本是 npm run start)
"start": "node index.js", 
  "test": "echo \"Error: no test specified\" && exit 1",
  "stop": "",
  "restart": ""
```

![image-20220223101750533](assets/nodejs学习.fig/image-20220223101750533.png)

**补充：webpack 在打包的时候，会从入口文件开始查找所有的依赖关系，生成一个依赖关系图，即便有一些依赖在devDependecies中，但是最后也会被打包**

**所以devDependecies的作用是当项目被作为工具** 



### 1.3 版本属性控制

![image-20220114162445302](assets/nodejs学习.fig/image-20220114162445302.png)

![image-20220224092029738](assets/nodejs学习.fig/image-20220224092029738.png)



## 2.npm install 命令 

![image-20220114164925061](assets/nodejs学习.fig/image-20220114164925061.png)

![image-20220114165424999](assets/nodejs学习.fig/image-20220114165424999.png)



### 2.1 --save -dev 和--production 的区别

 --save -dev 是在安装某一个单独的依赖的时候使用  --production 是安装所有开发时依赖，都是安装开发时依赖

### 2.2 npm install 的原理

![image-20220114165643758](assets/nodejs学习.fig/image-20220114165643758.png)

![image-20220114170311446](assets/nodejs学习.fig/image-20220114170311446.png)

![image-20220114202357331](assets/nodejs学习.fig/image-20220114202357331.png)

## 3.Node的其他管理工具-Yarn

![image-20220114202728293](assets/nodejs学习.fig/image-20220114202728293.png)

## 4.cnpm

![image-20220114203343380](assets/nodejs学习.fig/image-20220114203343380.png)

## 5.npx 命令的使用

![image-20220114205007634](assets/nodejs学习.fig/image-20220114205007634.png)

  