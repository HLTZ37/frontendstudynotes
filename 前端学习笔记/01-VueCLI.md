

# **Vue-CLI(command line interface,脚手架)**

# **一、Vue CLI 的使用**

## 1. 什么是Vue CLI（command-Line I1nterface）

<img src="assets/01-VueCLI.fig/image-20210720103719981.png" alt="image-20210720103719981" style="zoom: 67%;" />

## 2. Vue CLI 的使用

<img src="assets/01-VueCLI.fig/image-20210721111119307.png" alt="image-20210721111119307" style="zoom:67%;" />

![image-20210720110413444](assets/01-VueCLI.fig/image-20210720110413444.png)

 ![image-20210720111137418](assets/01-VueCLI.fig/image-20210720111137418.png)

![image-20210720112207669](assets/01-VueCLI.fig/image-20210720112207669.png)

![image-20210720113008898](assets/01-VueCLI.fig/image-20210720113008898.png)

**因为package.json中存放的是依赖包的大概的版本，package-lock.json 是这些版本的具体映射（符号解释：‘^’表示只有最最后一个位可变，可安装大于该最后一个位的版本；‘~’表示只有最最后两个位可变，可安装大于该最后两个位的版本；，比如^3.4.1,以上是可以安装大于的3.4.1的版本，但是还需要注意的是仅仅是3.4.x版本，x>=1;同理~3.4.1，表示可安装3.x.y,的版本，但是x>=4,y>=1）**

<img src="assets/01-VueCLI.fig/image-20210721110449380.png" alt="image-20210721110449380" style="zoom:67%;" />

<img src="assets/01-VueCLI.fig/image-20210719141802709.png" alt="image-20210719141802709" style="zoom:67%;" />

## 3. runtime-compiler 和 runtime-only

// 1. **runtime-compiler** 模式下 vue的程序执行过程（）：**template -> ast(抽象语法树) -> render -> virtual dom -> UI** 

// 2. **runtime-only** 模式下 vue的程序执行过程（）：**render -> virtual dom -> UI** 

// runtime-only (1.性能更高， 代码两更少)

## 4.Vue CLI3

![image-20210721110724024](assets/01-VueCLI.fig/image-20210721110724024.png)

rc -> run  recommand

<img src="assets/01-VueCLI.fig/image-20210721151718899.png" alt="image-20210721151718899" style="zoom: 67%;" />

![image-20210721153549135](assets/01-VueCLI.fig/image-20210721153549135.png)

![image-20210721153627649](assets/01-VueCLI.fig/image-20210721153627649.png)

# **二、Vue-router**

## 1. 路由    

### 1.1  什么是后端路由

​	后端路由一般由后端处理URL和页面之间的渲染关系，早期网页渲染是由php或者jsp（java servre page ，里面 代码主要是java+html+css完成，没有js） 开发

![image-20210721165952770](assets/01-VueCLI.fig/image-20210721165952770.png)

### 1.2  前后端分离

![image-20210721170814856](assets/01-VueCLI.fig/image-20210721170814856.png)

### 1.3  前端路由

![image-20210721202416375](assets/01-VueCLI.fig/image-20210721202416375.png)

## 2. 路由的安装和使用

![image-20210721215642281](assets/01-VueCLI.fig/image-20210721215642281.png)

### 2.2  url的hash 和html5的history

**修改网页路径但是不刷新页面的两种方式**

#### a.  url 的hash

​		**location.hash=""**

#### b.html5的history

​		**history.pushState(data,title,?url)**    // pushState()函数操作的是栈结构

​		**history.back()** // 表示出栈，不传参数，等价于history.go(-1)

​		**history.go(number)**	// number <0 表示出栈，大于0表示入栈

<img src="assets/01-VueCLI.fig/image-20210722112136631.png" alt="image-20210722112136631" style="zoom:67%;" />

## 3. 路由的默认路径和history模式

![image-20210722161222478](assets/01-VueCLI.fig/image-20210722161222478.png)

![image-20210722162209946](assets/01-VueCLI.fig/image-20210722162209946.png)

## 4.  router-link的其他属性补充

![image-20210721220646059](assets/01-VueCLI.fig/image-20210721220646059.png)

![image-20210722100300490](assets/01-VueCLI.fig/image-20210722100300490.png)

## 5. 路由懒加载的使用

![image-20210723104006238](assets/01-VueCLI.fig/image-20210723104006238.png)

![image-20210723104104738](assets/01-VueCLI.fig/image-20210723104104738.png)

![image-20210723104427949](assets/01-VueCLI.fig/image-20210723104427949.png)

## 6.路由的嵌套

 ![image-20211012154357849](assets/01-VueCLI.fig/image-20211012154357849.png)

![image-20211012155006203](assets/01-VueCLI.fig/image-20211012155006203.png)



## 7.路由参数的传递

![image-20210723200546646](assets/01-VueCLI.fig/image-20210723200546646.png)

**URL的组成：协议：//主机（或服务器）：端口/路径？查询       => scheme:// host:port/path/query#fragment**

### 7.1 query 类型的参数的传递以及获取

![image-20210723203920827](assets/01-VueCLI.fig/image-20210723203920827.png)

### 7.2 params 参数的传递和获取

![image-20210723204327637](assets/01-VueCLI.fig/image-20210723204327637.png)

### 7.3 参数 传递:用按钮事件点击的方式实现

![image-20210723204953420](assets/01-VueCLI.fig/image-20210723204953420.png)

### 7.4 $route和 $router的区别

![image-20210723210711466](assets/01-VueCLI.fig/image-20210723210711466.png)

### 7.5 导航守卫

![image-20210723212556467](assets/01-VueCLI.fig/image-20210723212556467.png)

![image-20210723212633259](assets/01-VueCLI.fig/image-20210723212633259.png)

![image-20211012165050317](assets/01-VueCLI.fig/image-20211012165050317.png)

## 8.keep-alive

![image-20211012172346021](assets/01-VueCLI.fig/image-20211012172346021.png)

**使用场景：让组件不被频繁创建created或者销毁distory**

**使用方式：将路由占用组建<router-view>用<keep-alive>标签包裹**

**并使用了path来记录离开时的路径**

![image-20211013110420616](assets/01-VueCLI.fig/image-20211013110420616.png)

**注意：actived()和deactived()函数只有该组件在被保持在keeo-alive标签中才起作用**

## 9.项目实战：Tabbar 的封装

​	![image-20210725123910841](assets/01-VueCLI.fig/image-20210725123910841.png)

### 注意点总结：

### 1.使用别名的方法：

在webpack.base.js 文件中 ，配置resolve下的alias属性

**使用的是会有需要注意的是，如若在属性中（DOM中）使用路径，需要钱路径前面加上‘~’,否则图片或者文件不显示，如：**

```
<img src="~assets/img/home-active.svg" alt="" slot="item-img-active" />  // 这里的src 是属性，所以要加上‘~’，调试是，发现图片一致不显示，最后才查明是因为没有加“~”
```

![image-20210725124452183](assets/01-VueCLI.fig/image-20210725124452183.png)

# **三、Vuex**

## 1.Vuex是什么

![image-20210726220038725](assets/01-VueCLI.fig/image-20210726220038725.png)

![image-20210727085329363](assets/01-VueCLI.fig/image-20210727085329363.png)

![image-20210727141650340](assets/01-VueCLI.fig/image-20210727141650340.png)

vuex 的安装命令 ：npm  install vuex  --save   // 跳转到需要的项目文件中进行安装（局部安装），也可以加上参数‘-g’变成全局安装

使用vuex，一般会建立store的文件夹，因为实际使用vuex 时，是用的vuex.stroe

**插件使用的一般的而是用步骤：**

1. 导入插件：import......   // 如：import Vuex from "vuex"
2. 安装插件： vue.use()   // use 内部使用其实是 install 命令
3. 创建对象：const  obj  = new Object({}) // 如：const store = new Vuex.Store({})
4. 导出对象：export  default obj // 如：export default st-ore;

## 2.Vuex原理图

![image-20210727111011453](assets/01-VueCLI.fig/image-20210727111011453.png)

## 3.单一状态树

![image-20210727143344151](assets/01-VueCLI.fig/image-20210727143344151.png) 

![image-20210801165257472](assets/01-VueCLI.fig/image-20210801165257472.png)



![image-20210801171841496](assets/01-VueCLI.fig/image-20210801171841496.png)

![image-20210801172449460](assets/01-VueCLI.fig/image-20210801172449460.png)

![image-20210802143350563](assets/01-VueCLI.fig/image-20210802143350563.png)![image-20210802143359369](assets/01-VueCLI.fig/image-20210802143359369.png)

## 4.Mutation的响应式规则

![image-20210801195141105](assets/01-VueCLI.fig/image-20210801195141105.png)

![image-20210801195422640](assets/01-VueCLI.fig/image-20210801195422640.png)

## 5.Mutation的同步函数

![image-20210801202145428](assets/01-VueCLI.fig/image-20210801202145428.png)

## 6.vuex中的actions

![image-20210801202211404](assets/01-VueCLI.fig/image-20210801202211404.png)

## 8.vuex中的Module

![image-20210801203848835](assets/01-VueCLI.fig/image-20210801203848835.png)



# **四、网络请求的封装**

## 1.JSONP

![image-20210802210023412](assets/01-VueCLI.fig/image-20210802210023412.png)

## 2.axios简介

![image-20210802210345596](assets/01-VueCLI.fig/image-20210802210345596.png)

![image-20210802210515181](assets/01-VueCLI.fig/image-20210802210515181.png)

![image-20210803183214847](assets/01-VueCLI.fig/image-20210803183214847.png)

![image-20211020153827286](assets/01-VueCLI.fig/image-20211020153827286.png)

## 3.全局配置

![image-20211020154825794](assets/01-VueCLI.fig/image-20211020154825794.png)

## 4.axios的常见配置选项

![image-20211020154713010](assets/01-VueCLI.fig/image-20211020154713010.png)

## 5.axios创建实例和模块封装

![image-20211020204041867](assets/01-VueCLI.fig/image-20211020204041867.png)



## 6.axios的拦截器

![image-20210803183420034](assets/01-VueCLI.fig/image-20210803183420034.png)

![image-20210803185343893](assets/01-VueCLI.fig/image-20210803185343893.png)

![image-20210803185113014](assets/01-VueCLI.fig/image-20210803185113014.png)

# **五、ES6知识点补充**

## 5.1 ES6简介

JavaScript 的创造者 `Netscape` 公司，决定将 JavaScript 提交给标准化组织 `ECMA`，希望这种语言能够成为国际标准。

ES6 既是一个历史名词，也是一个泛指，含义是 5.1 版以后的 JavaScript 的下一代标准，涵盖了 ES2015、ES2016、ES2017 等等，而 `ES2015` 则是`正式名称`，特指该年发布的正式版本的语言标准。本书中提到 ES6 的地方，一般是指 ES2015 标准。



## 5.2 Promise

![image-20210726154353144](assets/01-VueCLI.fig/image-20210726154353144.png)

![image-20210726192140090](assets/01-VueCLI.fig/image-20210726192140090.png)

### 5.2.1`Promise`解决的问题

1. 回调地狱问题

2. 可读性问题

3. 信任问题(也称控制反转问题)

   参考链接https://segmentfault.com/a/1190000023042995

### 5.2.2  promise的三种状态

![image-20210726164341505](assets/01-VueCLI.fig/image-20210726164341505.png)

![image-20210726164850602](assets/01-VueCLI.fig/image-20210726164850602.png)

![image-20210726212929006](assets/01-VueCLI.fig/image-20210726212929006.png)

# 六5.、git的使用，随记

## 1. 将远程仓库和本地文件，联系起来，不使用拷贝的办法

![image-20210803193512751](assets/01-VueCLI.fig/image-20210803193512751.png)





2







